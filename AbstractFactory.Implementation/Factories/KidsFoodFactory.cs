﻿using AbstractFactoryDemo.Shared;

namespace AbstractFactory.Implementation
{
    public class KidsFoodFactory : IFoodFactory
    {
        public Dessert CreateDessert()
        {
            return new ChocoTruffle();
        }

        public Sandwich CreateSandwich()
        {
            return new CheeseSandwich();
        }
    }
}
