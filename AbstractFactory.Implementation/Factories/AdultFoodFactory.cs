﻿using AbstractFactoryDemo.Shared;

namespace AbstractFactory.Implementation
{
    public class AdultFoodFactory : IFoodFactory
    {
        public Dessert CreateDessert()
        {
            return new CoconutIceCream();
        }

        public Sandwich CreateSandwich()
        {
            return new MexicanSaladSandwich();
        }
    }
}
