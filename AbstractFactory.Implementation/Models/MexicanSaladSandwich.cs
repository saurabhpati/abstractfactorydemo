﻿using AbstractFactoryDemo.Shared;

namespace AbstractFactory.Implementation
{
    public class MexicanSaladSandwich : Sandwich
    {
        public override string ToString()
        {
            return "Mexican salad sandwich for you";
        }
    }
}
