﻿using AbstractFactoryDemo.Shared;

namespace AbstractFactory.Implementation
{
    public class CoconutIceCream : Dessert
    {
        public override string ToString()
        {
            return "Coconut ice cream for you!";
        }
    }
}
