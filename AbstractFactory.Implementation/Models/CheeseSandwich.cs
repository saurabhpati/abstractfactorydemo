﻿using AbstractFactoryDemo.Shared;

namespace AbstractFactory.Implementation
{
    public class CheeseSandwich : Sandwich
    {
        public override string ToString()
        {
            return "A cheese sandwich for you!";
        }
    }
}
