﻿using AbstractFactoryDemo.Shared;

namespace AbstractFactory.Implementation
{
    public class ChocoTruffle : Dessert
    {
        public override string ToString()
        {
            return "Choco truffle for you!";
        }
    }
}
