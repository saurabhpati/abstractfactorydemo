﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AbstractFactoryDemo.Shared
{
    public interface IFoodFactory
    {
        Sandwich CreateSandwich();

        Dessert CreateDessert();
    }
}
