﻿using System;
using AbstractFactory.Implementation;
using AbstractFactoryDemo.Shared;

namespace AbstractFactoryDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Adult (Press 'a') or Kid (press 'k')?");
            var input = Console.ReadKey();
            Console.WriteLine();
            IFoodFactory foodFactory = null;

            switch (input.KeyChar)
            {
                case 'A':
                case 'a':
                    foodFactory = new AdultFoodFactory();
                    break;

                case 'K':
                case 'k':
                    foodFactory = new KidsFoodFactory();
                    break;

                default:
                    Console.WriteLine("Seriously? You are a disgrace!");
                    Console.WriteLine("Press any key to exit!");
                    Console.ReadKey();
                    Environment.Exit(0);
                    break;
            }

            var sandwich = foodFactory.CreateSandwich();
            var dessert = foodFactory.CreateDessert();

            Console.WriteLine("--------Breakfast is served--------");
            Console.WriteLine($"{Convert.ToString(sandwich)}{Environment.NewLine}{Convert.ToString(dessert)}");
            Console.WriteLine("--------Press any key to exit.--------");
            Console.ReadKey();
        }
    }
}
